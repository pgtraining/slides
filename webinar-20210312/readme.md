# Webinar 12 Marzo 2021

Breve resoconto dell'evento **online grauito** organizzato il giorno 12 Marzo 2021.

## Sharding (mediante Citus) - Enrico Pirozzi

Link utili:
- [video della presentazione](https://vimeo.com/520841922)
- [slideshare](https://www.slideshare.net/pgtraining/webminar-del-12032012-244374137)

## pgBackRest - Luca Ferrari
pgBackRest è una soluzione altamente efficiente per i backup di cluster PostgreSQL di grosse dimensioni, dell'ordine dei terabyte.
In questa breve presentazione vengono illustrati i concetti principali di pgBackRest, le feature e le motivazioni per scegliere questo tool come strumento di backup e restore.
Particolare attenzione viene posta circa la capacità di pgBackRest di effettuare *delta restore* e *backup incrementali* al fine di risparmiare risorse di rete e di storage per completare le operazioni di restore e backup.
Sono inoltre forniti alcuni esempi di base di configurazione.


Link utili:
- [video (registrato a posteriori)](https://vimeo.com/523432651)
- [slideshare](https://www.slideshare.net/pgtraining/webminar-del-12032012)


## Esperimenti con il JIT compiler - Chris Mair

Link utili:
- [video (registrato a posteriori)](https://vimeo.com/525756192)
- [slideshare](https://www.slideshare.net/pgtraining/weminar-12032021-jit)

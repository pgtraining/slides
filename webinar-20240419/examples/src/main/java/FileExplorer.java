package pgtraining;

import org.postgresql.pljava.*;
import org.postgresql.pljava.annotation.Function;
import static org.postgresql.pljava.annotation.Function.Effects.IMMUTABLE;
import static org.postgresql.pljava.annotation.Function.OnNullInput.RETURNS_NULL;
import org.postgresql.pljava.ResultSetProvider;

import java.util.*;
import java.util.stream.*;
import java.sql.SQLException;
import java.util.logging.*;
import java.sql.ResultSet;
import java.sql.Date;
import java.io.*;

public class FileExplorer implements ResultSetProvider {

    private final static Logger logger = Logger.getAnonymousLogger();

    private File directory;
    private String[] files;

    public FileExplorer( String path ) throws SQLException {
	super();
	directory = new File( path );

	if ( ! directory.exists() )
	    throw new SQLException( "Directory specificata non trovata!" );

	files = directory.list();
	logger.log( Level.INFO, String.format( "Directory [%s] con %d entries",
					       path,
					       files.length ) );
    }


    @Override
    public boolean assignRowValues( ResultSet tuples, int row )
	throws SQLException {

	if ( row >= files.length )
	    return false;

	File current = new File( String.format( "%s/%s",
						directory.getAbsolutePath(),
						files[ row ] ) );
	tuples.updateString( 2, current.getName() );
	tuples.updateString( 1, current.getAbsolutePath() );
	tuples.updateLong( 3, current.length() );
	tuples.updateString( 4, current.isDirectory() ? "d" : "f" );
	return true;
    }

    @Override public void close() { }

    @Function( schema = "pgtraining",
	       name   = "file_explorer",
	       out    = { "p text", "n text", "s bigint", "t char" },
	       trust  = Function.Trust.UNSANDBOXED // language = "javau"
	      )
    public static final ResultSetProvider init_file_explorer( String path ) throws SQLException {
	logger.log( Level.INFO, "Entering Java function FileExplorer.init_file_explorert()" );

	return new FileExplorer( path );
    }
}

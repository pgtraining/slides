package pgtraining;

import org.postgresql.pljava.*;
import org.postgresql.pljava.annotation.Function;
import static org.postgresql.pljava.annotation.Function.Effects.IMMUTABLE;
import static org.postgresql.pljava.annotation.Function.OnNullInput.RETURNS_NULL;

import java.util.*;
import java.util.stream.*;
import java.sql.SQLException;
import java.util.logging.*;
import java.sql.ResultSet;
import java.sql.Date;
import java.util.regex.*;
import java.sql.*;

import org.postgresql.pljava.SessionManager;
import org.postgresql.pljava.TriggerData;
import org.postgresql.pljava.TriggerException;
import org.postgresql.pljava.annotation.*;
import static org.postgresql.pljava.annotation.Trigger.Called.*;
import static org.postgresql.pljava.annotation.Trigger.Constraint.*;
import static org.postgresql.pljava.annotation.Trigger.Event.*;
import static org.postgresql.pljava.annotation.Trigger.Scope.*;

public class FileTableInsert {

    private final static Logger logger = Logger.getAnonymousLogger();

    @Function(
	      schema = "pgtraining",
	      name   = "insert_files"
	      )
    public final static int insert_file_from_name( String files[] )
    throws SQLException {

	if ( files == null || files.length == 0 )
	    throw new SQLException( "Devi specificare dei nomi di file" );

	int done = 0;

	// preparazione della connessione
	Connection database = DriverManager
	    .getConnection("jdbc:default:connection");
	PreparedStatement query = database
				.prepareStatement("INSERT INTO files( file_storage, file_path, file_name, file_ext ) "
						  + " VALUES( ?, ?, ?, ? )" );

	for ( String current : files ) {
	    String data[] = explode( current );
	    for ( int i = 0; i < data.length; i++ )
		query.setString( i + 1, data[ i ] );

	    query.executeUpdate();
	    done++;
	}


	return done;
    }



    private static final String[] explode( String file ) throws SQLException {
		if ( file == null || file.isEmpty() )
	    throw new SQLException( "Devi specificare un file/directory" );



	String storage, path, ext;
	storage = path = ext = null;


	Pattern windows_pattern = Pattern.compile( "^((?<storage>[A-Za-z]):)(?<path>.*)$" );
	Matcher matcher = windows_pattern.matcher( file );
	if ( matcher.find() ) {
	    storage = matcher.group( "storage" );
	    path    = matcher.group( "path" );
	}
	else {
	    storage = null;
	    path    = file;
	}

	path = path.replace( "\\", "/" );
	boolean isFile = ! path.endsWith( "/" );

	if ( isFile ) {
	    String parts[] = path.split( "/" );
	    path    = String.join( "/",
				   Arrays.stream( parts, 0, parts.length - 1 )
				   .toArray( String[]::new ) );
	    file    = parts[ parts.length - 1 ];
	    parts   = file.split( "[.]" );
	    ext     = parts.length > 1 ?  parts[ parts.length - 1 ] : null;


	}
	else {
	    file = null;
	    ext  = null;
	}

	logger.log( Level.INFO, String.format( "Storage [%s], Path [%s], file ? %b ([%s] ext [%s])",
					       storage, path,
					       isFile, file, ext ) );


	String[] output = new String[ 4 ];
	output[ 0 ] = storage;
	output[ 1 ] = path;
	output[ 2 ] = file;
	output[ 3 ] = ext;
	return output;
    }

}




package pgtraining;

import org.postgresql.pljava.*;
import org.postgresql.pljava.annotation.Function;
import static org.postgresql.pljava.annotation.Function.Effects.IMMUTABLE;
import static org.postgresql.pljava.annotation.Function.OnNullInput.RETURNS_NULL;

import java.util.*;
import java.util.stream.*;
import java.sql.SQLException;
import java.util.logging.*;
import java.sql.ResultSet;
import java.sql.Date;

public class Greetings {

    private final static Logger logger = Logger.getAnonymousLogger();

    @Function( schema = "pgtraining",
	       effects = IMMUTABLE )
    public static final String greetings_pljava( String who ) throws SQLException {
	logger.log( Level.INFO, "Entering Java function greetings.Greetings.greetings_pljava()" );

	if ( who == null || who.length() <= 0 )
	    return "Hello World!";
	else
	    return String.format( "Hello %s, welcome to PL/Java", who );

	// ... or ...
	// return "Hello " + ( who != null ? who : "World!" );
    }
}

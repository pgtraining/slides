


package pgtraining;

import org.postgresql.pljava.*;
import org.postgresql.pljava.annotation.Function;
import static org.postgresql.pljava.annotation.Function.Effects.IMMUTABLE;
import static org.postgresql.pljava.annotation.Function.OnNullInput.RETURNS_NULL;

import java.util.*;
import java.util.stream.*;
import java.sql.SQLException;
import java.util.logging.*;
import java.sql.ResultSet;
import java.sql.Date;
import java.util.regex.*;

public class CodiceFiscale {

    private final static Logger logger = Logger.getAnonymousLogger();
    private final static String cfRegexp = "^[A-Z]{6}\\d{2}[A-Z]\\d{2}[A-Z]\\d{3}[A-Z]$";

    @Function( schema = "pgtraining",
	       onNullInput = RETURNS_NULL,
	       effects = IMMUTABLE )
    public static final boolean checkCodiceFiscale( String cf ) throws SQLException {
	logger.log( Level.INFO, "Entering Java function CodiceFiscale.checkCodiceFiscale()" );

	if ( cf == null || cf.trim().length() < 16 )
	    return false;

	logger.log( Level.INFO,
		    String.format( "Checking [%s] against [%s]", cfRegexp, cf ) );
	Pattern pattern = Pattern.compile( cfRegexp );
	Matcher matcher = pattern.matcher( cf );
	return matcher.find();
    }
}

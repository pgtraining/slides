# Webinar 19 Aprile 2024

Breve resoconto dell'evento **online grauito** organizzato il giorno 19 Aprile 2024.

<img src="illustration-2024.png" title="an elephant is juggling version numbers, a moka pot of Java and some vectors" alt="an elephant is juggling version numbers, a moka pot of Java and some vectors" width="256" height="256"></a>


[Presentazione dei talk](https://gitlab.com/pgtraining/slides/-/blob/master/webinar-20240419/2024_PGTRAINING_20240419.pdf)

## Introduzione al linguaggio PL/Java - Luca Ferrari

PL/Java consente di utilizzare la tecnologia Java direttamente dentro a PostgreSQL, per la scrittura di funzioni, trigger e procedure.

Link utili:
- [video su Vimeo](https://vimeo.com/manage/videos/938072562) &check;
- [slide PDF](https://gitlab.com/pgtraining/slides/-/blob/master/webinar-20240419/2024_PGTRAINING_PLJAVA.pdf?ref_type=heads) &check;
- [file di esempio](https://gitlab.com/pgtraining/slides/-/tree/master/webinar-20240419/examples/src/main/java?ref_type=heads) &check;


## Introduzione a PgVector - Chris Mair

Link utili:
- [slide PDF](https://gitlab.com/pgtraining/slides/-/blob/master/webinar-20240419/2024_PGTRAINING_pgvector.pdf) &check;
- [video su Vimeo](https://vimeo.com/936708513) &check;
- [estratto 1K/6.7M della tabella usata ed esempi codice](https://gitlab.com/pgtraining/slides/-/tree/master/webinar-20240419/examples-pgvector/) &check;


## Repliche logiche e migrazione di versione a caldo da PostgreSQL 12 a PostgreSQL 16 - Enrico Pirozzi

Link utili:
- [slide PDF](https://gitlab.com/pgtraining/slides/-/blob/master/webinar-20240419/2024_PGTRAINING_HOT_UPGRADE.pdf) &check;
- [video su Vimeo - Parte 1](https://vimeo.com/939131997) &check;
- [video su Vimeo - Parte 2](https://vimeo.com/944002619) &check;

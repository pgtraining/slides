# Introduzione a PgVector - Chris Mair
# https://gitlab.com/pgtraining/slides/-/tree/master/webinar-20240419#introduzione-a-pgvector-chris-mair
# 
# query_ix.py - 
# copia di query.py con la query modificata in modo di usare l'indice
#


# https://www.sbert.net/docs/pretrained_models.html#image-text-models
# https://huggingface.co/sentence-transformers/clip-ViT-L-14

# pip install sentence-transformers
# pip install psycopg2-binary

import time
import numpy as np
import psycopg2
import psycopg2.extras
from PIL import Image
from sentence_transformers import SentenceTransformer

cursor = psycopg2.connect("host='127.0.0.1' dbname='wiki' user='chris'").cursor(cursor_factory=psycopg2.extras.DictCursor)
cursor.execute("SET max_parallel_workers = 8")
cursor.execute("SET max_parallel_workers_per_gather = 8")

clip = SentenceTransformer('clip-ViT-L-14')


# f = clip.encode(Image.open("astroboy.jpg"));
# f = clip.encode(Image.open("warbler.jpg"));
# f = clip.encode(Image.open("pisa.jpg"));
# f = clip.encode(Image.open("pgalien.png"));

f = clip.encode("interesting places for sightseeing in the capital of France")

t0 = time.time()
cursor.execute(
    """
    select id, title || coalesce(' - ' || short_description, '') as description,
    1.0 - (embedding <=> %s::vector) as sim 
    from wiki_extract order by embedding <=> %s::vector limit 10
    """, [f.tolist(), f.tolist()])
results = cursor.fetchall()
t1 = time.time()

print("%.3f s" % (t1 - t0))
for result in results:
    print("%6.4f, %d, %s" % (result["sim"], result["id"], result["description"]))

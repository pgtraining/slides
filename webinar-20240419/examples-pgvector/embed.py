# Introduzione a PgVector - Chris Mair
# https://gitlab.com/pgtraining/slides/-/tree/master/webinar-20240419#introduzione-a-pgvector-chris-mair
# 
# embed.py - 
# aggiunge l'embedding alla tabella con i titoli e le descrizioni brevi degli articoli di Wikipedia
#


# https://www.sbert.net/docs/pretrained_models.html#image-text-models
# https://huggingface.co/sentence-transformers/clip-ViT-L-14

# pip install sentence-transformers
# pip install psycopg2-binary

import time
import numpy as np
import psycopg2
import psycopg2.extras
from sentence_transformers import SentenceTransformer

cursor = psycopg2.connect("host='127.0.0.1' dbname='wiki' user='chris'").cursor(cursor_factory=psycopg2.extras.DictCursor)

clip = SentenceTransformer('clip-ViT-L-14')

batch_size = 1000
cnt = 0
id = 1
while True:
    t0 = time.time()
    cursor.execute("select id, title || coalesce(' - ' || short_description, '') as description from wiki_extract where id between %s and %s;", [id, id + batch_size - 1])
    results = cursor.fetchall()
    if len(results) == 0:
        break;
    embed_ids = []
    embed_texts = []
    for result in results:
        print(result)
        embed_ids.append(result["id"])
        embed_texts.append(result["description"])
    t_select = time.time() - t0;
    t0 = time.time()
    embed_vector = clip.encode(embed_texts)
    t_embed = time.time() - t0;
    t0 = time.time()
    for i in range(0, len(results)):
        cursor.execute("update wiki_extract set embedding = %s where id = %s", [embed_vector[i].tolist(), embed_ids[i]])
    cursor.connection.commit()
    t_store = time.time() - t0;
    print("%d done, last batch: %.3f select, %.3f embed, %.3f insert" % (cnt, t_select, t_embed, t_store))
    cnt += len(results)
    id += batch_size

print("%d embeddings done" % (cnt));

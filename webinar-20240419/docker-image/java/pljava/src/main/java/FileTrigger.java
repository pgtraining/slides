package pgtraining;

import org.postgresql.pljava.*;
import org.postgresql.pljava.annotation.Function;
import static org.postgresql.pljava.annotation.Function.Effects.IMMUTABLE;
import static org.postgresql.pljava.annotation.Function.OnNullInput.RETURNS_NULL;

import java.util.*;
import java.util.stream.*;
import java.sql.SQLException;
import java.util.logging.*;
import java.sql.ResultSet;
import java.sql.Date;
import java.util.regex.*;

import org.postgresql.pljava.SessionManager;
import org.postgresql.pljava.TriggerData;
import org.postgresql.pljava.TriggerException;
import org.postgresql.pljava.annotation.*;
import static org.postgresql.pljava.annotation.Trigger.Called.*;
import static org.postgresql.pljava.annotation.Trigger.Constraint.*;
import static org.postgresql.pljava.annotation.Trigger.Event.*;
import static org.postgresql.pljava.annotation.Trigger.Scope.*;

public class FileTrigger {

    private final static Logger logger = Logger.getAnonymousLogger();

    @Function(
	      schema = "pgtraining",
	      name   = "disassemble_file_name"
	      , triggers = { @Trigger( called = BEFORE,
				       table  = "files",
				       events = { INSERT, UPDATE },
				       scope  = ROW ) } )
    public final static void disassemble_file_name( TriggerData trigger )
    throws SQLException {
	if( trigger.isFiredForStatement() )
	    throw new TriggerException( trigger, "Non attivabile a livello di statement!" );

	if( trigger.isFiredAfter() )
	    throw new TriggerException( trigger, "Non attivabile per AFTER" );

	ResultSet newResultSet = trigger.getNew();

	// se questa tupla ha gia' estensione e percorso non c'è nulla da fare
	if ( newResultSet.getString( "file_ext" ) != null
	     && ! newResultSet.getString( "file_ext" ).isEmpty()
	     && newResultSet.getString( "file_path" ) != null
	     && ! newResultSet.getString( "file_path" ).isEmpty() )
	    return;

	String path, storage, file, ext;
	path = storage = file = ext = null;

	file    = newResultSet.getString( "file_name" );


	Pattern windows_pattern = Pattern.compile( "^((?<storage>[A-Za-z]):)(?<path>.*)$" );
	Matcher matcher = windows_pattern.matcher( file );
	if ( matcher.find() ) {
	    // percorso Windows
	    storage = matcher.group( "storage" );
	    path    = matcher.group( "path" );
	}
	else {
	    storage = null;
	    path    = file;
	}

	path = path.replace( "\\", "/" );
	boolean isFile = ! path.endsWith( "/" );

	if ( isFile ) {
	    String parts[] = path.split( "/" );
	    path    = String.join( "/",
				   Arrays.stream( parts, 0, parts.length - 1 )
				   .toArray( String[]::new ) );
	    file    = parts[ parts.length - 1 ];
	    parts   = file.split( "[.]" );
	    ext     = parts.length > 1 ?  parts[ parts.length - 1 ] : null;


	}
	else {
	    file = null;
	    ext  = null;
	}

	logger.log( Level.INFO, String.format( "Storage [%s], Path [%s], file ? %b ([%s] ext [%s])",
					       storage, path,
					       isFile, file, ext ) );

	// aggiornamento dei dati della tupla
	newResultSet.updateString( "file_name", file );
	newResultSet.updateString( "file_storage", storage );
	newResultSet.updateString( "file_path", path );
	newResultSet.updateString( "file_ext", ext );
    }




}

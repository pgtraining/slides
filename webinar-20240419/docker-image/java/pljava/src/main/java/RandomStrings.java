package pgtraining;

import org.postgresql.pljava.*;
import org.postgresql.pljava.annotation.Function;
import static org.postgresql.pljava.annotation.Function.Effects.IMMUTABLE;
import static org.postgresql.pljava.annotation.Function.OnNullInput.RETURNS_NULL;
import org.postgresql.pljava.ResultSetProvider;

import java.util.*;
import java.util.stream.*;
import java.sql.SQLException;
import java.util.logging.*;
import java.sql.ResultSet;
import java.sql.Date;

public class RandomStrings implements ResultSetProvider {

    private final static Logger logger = Logger.getAnonymousLogger();

    private final static String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	+ "abcdefghijklmnopqrstuvwxz"
	+ "0123456789"
	+ "!$%&?#";


    private int quantity;
    private int size;

    public RandomStrings( int minLength, int howMany ) {
	super();
	quantity = howMany > 0 ? howMany : 10;    // 10 stringhe in default
	size     = minLength > 0 ? minLength : 8; // almeno 8 caratteri
    }


    @Override
    public boolean assignRowValues( ResultSet tuples, int row )
	throws SQLException {

	if ( row >= quantity )
	    return false;

	tuples.updateString( 1, generateRandomText() );
	tuples.updateString( 2, generateRandomText() );
	return true;
    }

    private final String generateRandomText() {
	StringBuilder builder = new StringBuilder();
	Random engine = new Random();
	int remaining = size;

	while ( remaining > 0 ) {
	    builder.append( alphabet.charAt( engine.nextInt( alphabet.length() ) ) );
	    remaining--;
	}

	return builder.toString();

    }

    @Override public void close() { }

    @Function( schema = "pgtraining",
	       name   = "random_text",
	       out    = { "r1 text", "r2 text" },
	       effects = IMMUTABLE )
    public static final ResultSetProvider random_text( int minLength, int howMany ) throws SQLException {
	//	logger.log( Level.INFO, "Entering Java function RandomStrings.random_text()" );

	return new RandomStrings( minLength, howMany );
    }
}

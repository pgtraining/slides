set -e
psql -U postgres -c "create database java with owner java;" postgres

psql -U postgres -c "set pljava.libjvm_location to '/usr/lib/jvm/java-17-openjdk-amd64/lib/server/libjvm.so';" java

psql -U postgres -c "CREATE EXTENSION pljava;" java
psql -U postgres -c "CREATE EXTENSION plperl;" java


psql -U java -c "CREATE SCHEMA pgtraining;" java




psql -U java <<'EOF'
    CREATE TABLE files(
    pk int generated always as identity
   , file_name    text
   , file_ext     text
   , file_path    text
   , file_storage text
   , PRIMARY KEY( pk )
   , UNIQUE ( file_storage, file_path, file_name, file_ext )
);

EOF



psql -U java <<'EOF'
 CREATE OR REPLACE FUNCTION greetings( text DEFAULT NULL )
 RETURNS text
 AS $CODE$
    my ( $who ) = @_;

    return "Hello $who" if defined( $who ) && $who;
    return "Hello World!";
 $CODE$
 LANGUAGE plperl;
EOF

psql -U postgres -c 'CREATE EXTENSION IF NOT EXISTS bool_plperl;' java


psql -U java <<'EOF'
 CREATE OR REPLACE FUNCTION
 check_codice_fiscale( text )
 RETURNS bool
 TRANSFORM FOR TYPE bool
 AS $CODE$
   $_[0] =~ /^ [A-Z]{6} \d{2} [A-Z] \d{2} [A-Z] \d{3} [A-Z] $/xi;
 $CODE$
 LANGUAGE plperl;

EOF


psql -U java <<'EOF'
CREATE OR REPLACE FUNCTION
generate_random_strings( int DEFAULT 8, int DEFAULT 5 )
RETURNS SETOF text
AS $CODE$

   my ( $string_length, $limit ) = @_;
   my @alphabet = ( 'A' .. 'Z' , 0 .. 9, 'a' .. 'z', qw/ ! $ % & # / );
   while ( $limit > 0 ) {
      my @data = map { $alphabet[ int(rand($#alphabet) ) ] }
                                     ( 0 .. $string_length );
     return_next( join( '', @data ) );
     $limit--;
   }

   return;
$CODE$
LANGUAGE plperl;

EOF



psql -U postgres -c "GRANT ALL ON SCHEMA sqlj TO PUBLIC;" java

set -e
psql -U postgres -c "alter role postgres password 'PgTraining20240419'" postgres
psql -U postgres -c "create role luca with login password  'PgTraining20240419' connection limit 5" postgres
psql -U postgres -c "create role java with login password  'PgTraining20240419' connection limit 5" postgres
psql -U postgres -c "create role enrico with login password  'PgTraining20240419' connection limit 5" postgres
psql -U postgres -c "create role chris with login password  'PgTraining20240419' connection limit 5" postgres

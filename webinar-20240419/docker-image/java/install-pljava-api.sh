#!/bin/sh

VERSION=1_6_6
cd $HOME

if [ ! -d pljava-${VERSION} ]; then
    wget https://github.com/tada/pljava/archive/refs/tags/V${VERSION}.tar.gz
    tar xzvf V${VERSION}.tar.gz
fi

cd pljava-${VERSION}/ && mvn clean install

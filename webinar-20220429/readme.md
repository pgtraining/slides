# Webinar 29 Aprile 2022

Breve resoconto dell'evento **online grauito** organizzato il giorno 29 Aprile 2022.

## Gestione del pool con Pgagroal - Luca Ferrari

[pgagroal](https://github.com/agroal/pgagroal){:target="blank"} è un connection pooler specifico per PostgreSQL che richiede poche risorse ed è ad alte performance.

Link utili:
- [video su Vimeo](https://vimeo.com/704626463){:target="blank"}
- [slide PDF](https://gitlab.com/pgtraining/slides/-/blob/master/webinar-20220429/2022_PGTRAINING_PGAGROAL.pdf)
- [file di configurazione di esempio](https://gitlab.com/pgtraining/slides/-/tree/master/webinar-20220429/conf/pgagroal)
- [pgagroal](https://github.com/agroal/pgagroal)

## Gestione time series con l'extension TimescaleDB - Chris Mair

Link utili:
- [video su Vimeo](https://vimeo.com/manage/videos/704609655){:target="_blank"}
- [appunti su Gitlab](https://gitlab.com/pgtraining/slides/-/blob/master/webinar-20220429/2022_PGTRAINING_TIMESCALEDB-v1.0.pdf)


## Storage su PostgreSQL e column storage con Citus - Enrico Pirozzi

Link utili:
- video pending...
- appunti pending...

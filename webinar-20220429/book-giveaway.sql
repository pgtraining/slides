CREATE TABLE IF NOT EXISTS participants
(
   id_ordine   bigint,
   nome text,
   cognome text,
   email text
);

BEGIN;
TRUNCATE participants;
\COPY participants FROM 'participants.csv' WITH ( FORMAT CSV,  DELIMITER ',', HEADER );
ALTER TABLE participants ADD COLUMN excluded boolean DEFAULT false;
ALTER TABLE participants ADD COLUMN winner   boolean DEFAULT false;
ALTER TABLE participants ADD CONSTRAINT id_ordine_unique UNIQUE( id_ordine );

UPDATE participants
SET excluded = true
WHERE
 ( cognome ilike 'mair' AND nome ilike 'chris' )
 OR
 ( cognome ilike 'ferrari' AND nome ilike 'luca' )
 OR
 ( cognome ilike 'pirozzi' AND nome ilike 'enrico' )
RETURNING cognome, nome, email;

COMMIT;


/*
WITH select_winners AS
(
  SELECT id_ordine
  FROM   participants
  WHERE  excluded = false
    AND  winner   = false
  ORDER BY random()
  LIMIT 3
)
UPDATE participants
SET    winner = true
WHERE  id_ordine IN ( SELECT id_ordine
                      FROM select_winners )
RETURNING upper( nome ), upper( cognome ),
          substr( email, 0, strpos( email, '@' ) + 1 ) || 'xxxxx';
*/

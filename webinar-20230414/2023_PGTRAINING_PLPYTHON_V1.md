> PgTraining online session 2023-04  
> 14 Aprile 2023  
> Enrico Pirozzi, Chris Mair, Luca Ferrari  
> https://gitlab.com/pgtraining/slides/-/tree/master/webinar-20230414

# Esperimenti con PL/Python

(C) Chris Mair  
Attribution 4.0 International (CC BY 4.0)  
https://creativecommons.org/licenses/by/4.0/

Contenuti:

- [Introduzione](#introduzione)
- [Primo esempio](#primo-esempio)
- [Overhead di chiamata](#overhead-di-chiamata)
- [Datatype mapping](#datatype-mapping)
- [Eseguire query da PL/Python](#eseguire-query-da-plpython)
- [Eseguire query da PL/Python: esempio più complesso](#eseguire-query-da-plpython-esempio-più-complesso)
- [Trigger](#trigger)
- [Escaping the sandbox](#escaping-the-sandbox)
- [Librerie di terze parti](#librerie-di-terze-parti)


## Introduzione

Cfr.: https://www.postgresql.org/docs/15/plpython.html

PL/Python permette di scrivere procedure (e quindi anche trigger) in Python.

Va installato come estensione (plpython3u).

La "u" sta per untrusted: il codice python non è sandboxed e può fare qualsiasi cosa
che l'utente Linux che esegue il servizio Postgres può fare (quindi tutto).

Di consequenza, solo superuser Postgres possono creare questa estensione in un
database e definire funzioni PL/Python.

Un notevole corollario di questo fatto è che non potete usare PL/Python in sistemi PaaS.
Per esempio AWS RDS/Postgres non lo offre!

Per creare l'estensione:

```text
pgtraining=# create extension plpython3u;
CREATE EXTENSION

pgtraining=# \dx
                         List of installed extensions
    Name    | Version |   Schema   |                Description                
------------+---------+------------+-------------------------------------------
 plpgsql    | 1.0     | pg_catalog | PL/pgSQL procedural language
 plpython3u | 1.0     | pg_catalog | PL/Python3U untrusted procedural language
(2 rows)
```

## Primo esempio

Ecco una prima funzione:

```text
CREATE OR REPLACE FUNCTION greet(name varchar)
  RETURNS varchar
AS $$
    return "Ciao " + name + "!";
$$ LANGUAGE plpython3u;
```

con questo risultato:

```text
pgtraining=# select greet('Chris');
    greet    
-------------
 Ciao Chris!
(1 row)
```

## Overhead di chiamata

Si pone subito la domanda: qual'è l'overhead di una chiamata di funzione in PL/Python piuttosto
di PL/PgSQL?

Sorprendentemente poco!

Confrontiamo la greet in Python con la greetSQL in PL/PgSQL:

```text
CREATE OR REPLACE FUNCTION greetSQL(name varchar)
  RETURNS varchar
AS $$
    BEGIN
      RETURN 'Ciao ' || name || '!';
    END;
$$ LANGUAGE plpgsql;
```

E chiamiamo entrambe 100k volte:

```text
\timing on
```

```text
DO $$
    BEGIN
        for i in 1..100000 loop
            perform greetSQL('Chris' || i);
        end loop;
    END;
$$ LANGUAGE plpgsql;  -- PL/PgSQL ~ 2.18 µs / chiamata
```

```text
DO $$
    BEGIN
        for i in 1..100000 loop
            perform greet('Chris' || i);
        end loop;
    END;
$$ LANGUAGE plpgsql;  -- Python ~ 2.62 µs / chiamata
```

C'e` un fattore ~ 1.20 di overhead, ma il tempo è talmente basso che difficilmente
farà una differenza nel caso di funzioni che andranno a eseguire query nel database.


## Datatype mapping

Cfr.: https://www.postgresql.org/docs/15/plpython-data.html

PostgreSQL      | PL/Python
----------------|-----------
boolean         | bool
tutti gli int   | int
float4, float8  | float
numeric         | decimal
bytea           | bytes
tutto il resto  | str
NULL            | None
array           | list
type/row        | dictionary 


## Eseguire query da PL/Python

Cfr.: https://www.postgresql.org/docs/15/plpython-database.html

Ecco una tabella esempio:

```text
CREATE TABLE wiki (
    id integer PRIMARY KEY,
    title varchar
);
```

Su questa tabella ho caricato i 5506784 titoli degli articoli della
Wikipedia inglese di qualche anno fa.

Ed ecco una funzione che restituisce un numero a partire da un parametro
(il numero di titoli che contengono una data stringa):

```text
CREATE OR REPLACE FUNCTION count_titles(name varchar)
  RETURNS int
AS $$
    if name == None:
        return None
    res = plpy.execute("SELECT count(*) AS cnt FROM wiki WHERE title ILIKE '%%%s%%'" % name)
    return res[0]["cnt"]
$$ LANGUAGE plpython3u;
```

Notare la strage di segni percento: il doppio `%` è l'escape Python del '%' che ci serve
per l'`ILIKE` e il '%s' è il placeholder.

Il modulo `plpy` è automaticamente importato dall'ambiente e mette a disposizione
funzioni per eseguire query.

In questo caso tutti i risultati vengono letti in res, che è una lista di dictionary.

Vediamo che per esempio ci sono 7 articoli che hanno OpenBSD nel titolo:

```text
pgtraining=# SELECT count_titles('openbsd');
 count_titles 
--------------
            7
(1 row)

Time: 2.151 ms
```


## Eseguire query da PL/Python: esempio più complesso

Facciamo qualcosa di più complesso una funzione che restituisce una tabella
(RETURNS SETOF):

```text
CREATE OR REPLACE FUNCTION get_titles(name varchar)
  RETURNS SETOF wiki
AS $$
    if name == None:
        return None
    res = plpy.execute("select id, title from wiki where title ilike '%%%s%%'" % name)
    return res
$$ LANGUAGE plpython3u;
```

Notare come usiamo il nome della tabella (*wiki*) come tipo di dato composto. Questo
è una tecnica comune anche in PL/PgSQL.

Ecco il risultato:

```text
pgtraining=# SELECT * FROM get_titles('openbsd');
   id    |              title              
---------+---------------------------------
  812378 | OpenBSD security features
  824812 | Absolute OpenBSD
  846339 | OpenBSD version history
 1732848 | OpenBSD Foundation
 2340406 | OpenBSD
 3697020 | OpenBSD Journal
 3861873 | OpenBSD Cryptographic Framework
(7 rows)

Time: 3.603 ms
```

Attenzione, questo modo di scrivere le cose non è memory-efficient. Se cerchiamo
una strings che ci dà troppi risultati, `res` potrebbe consumare tutta la memoria:

```text
SELECT COUNT(*) FROM get_titles(''); -- attenzione, consuma un sacco di RAM
```

Ci vuole un cursore!

```text
CREATE OR REPLACE FUNCTION get_titles(name varchar)
  RETURNS SETOF wiki
AS $$
    batch_size = 1000;
    if name == None:
        return None
    cursor = plpy.cursor("select id, title from wiki where title ilike '%%%s%%'" % name)
    while True:
        rows = cursor.fetch(batch_size)
        if not rows:
            break
        for row in rows:
            yield row
$$ LANGUAGE plpython3u;
```

Dà lo stesso output per 

```text
SELECT * FROM get_titles('openbsd');
```

ma, ora, restituisce tiene lato server soltanto al massimo 1000 row in memoria e quindi
questa query non rischia di esaurire la RAM:

```text
pgtraining=# SELECT COUNT(*) FROM get_titles(''); 
  count  
---------
 5506784
(1 row)

Time: 11958.630 ms (00:11.959)
```

C'è anche un `plpy.prepare()` che permette di creare prepared statements con i 
classici placeholder (`$1`, `$2`, ...) con i valori da passare all'`plpy.execute()`.

Notare l'uso di `yield`, che qui fa la parte del `RETURN NEXT` di PL/PgSQL.


## Trigger

Cfr.: https://www.postgresql.org/docs/15/plpython-trigger.html

Prendiamo un'altra tabella esempio:

```text
CREATE TABLE product (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL,
    price NUMERIC(8,2) NOT NULL
);

INSERT INTO product VALUES
(1, 'matita', 1.00),
(2, 'biro',   2.00);
```

Vogliamo scrivere un trigger BEFORE UPDATE che vieta l'aggiornamento del prezzo
se il nuovo valore si scosta più di un fattore 2 del vecchio prezzo.

```text
CREATE OR REPLACE FUNCTION check_tf()
  RETURNS TRIGGER
AS $$
    f = TD["new"]["price"] / TD["old"]["price"]
    if f == None or f < 0.5 or f > 2.0:
        plpy.error("invalid price change")
    return "OK"
$$ LANGUAGE plpython3u;

CREATE TRIGGER check_t
BEFORE UPDATE ON product
FOR EACH ROW
EXECUTE PROCEDURE check_tf();
```

Con questo trigger questo update fallisce:

```text
pgtraining=# UPDATE product SET price = 0.30 WHERE id = 1;
ERROR:  plpy.Error: invalid price change
CONTEXT:  Traceback (most recent call last):
  PL/Python function "check_tf", line 4, in <module>
    plpy.error("invalid price change")
PL/Python function "check_tf"
Time: 0.768 ms
```

Mentre questo va a buon fine:

```text
pgtraining=# UPDATE product SET price = 0.70 WHERE id = 1;
UPDATE 1
Time: 4.879 ms
```


## Escaping the sandbox

In PL/Python possiamo fare quello che vogliamo. Non siamo chiusi nella sandbox di PL/PgSQL.

Qui c'è un esempio dove usiamo Python per fare una ricerca di un titolo su Wikipedia via WWW e ottenerne l'estratto:

```text
CREATE OR REPLACE FUNCTION wikiextract(title text)
  RETURNS text
AS $$
    import urllib.request
    import urllib.parse
    import json
    prefix = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&"
    data = urllib.request.urlopen("%s&titles=%s" % (prefix, urllib.parse.quote(title))).read()
    doc = json.loads(data.decode('utf-8'))
    pages = list((doc["query"]["pages"]).keys())
    page = pages[0]
    return (doc["query"]["pages"][page]["extract"]).strip()
$$ LANGUAGE plpython3u;
```

Esempio d'uso (l'uso di SUBSTR() è solo per tenere l'output breve):

```text
pgtraining=# SELECT SUBSTR(wikiextract('openbsd'), 1, 80);
                                      substr                                      
----------------------------------------------------------------------------------
 OpenBSD is a security-focused, free and open-source, Unix-like operating system 
(1 row)

Time: 163.922 ms
```

E come tutte le funzioni custom, possiamo usarla in qualsiasi query:

```text
pgtraining=# SELECT title, SUBSTR(wikiextract(title), 1, 50) AS extract FROM get_titles('openbsd');
              title              |                      extract                       
---------------------------------+----------------------------------------------------
 OpenBSD security features       | The OpenBSD operating system focuses on security a
 Absolute OpenBSD                | Absolute OpenBSD: Unix for the Practical Paranoid 
 OpenBSD version history         | OpenBSD is a security-focused, free and open-sourc
 OpenBSD Foundation              | OpenBSD is a security-focused, free and open-sourc
 OpenBSD                         | OpenBSD is a security-focused, free and open-sourc
 OpenBSD Journal                 | The OpenBSD Journal is an online newspaper dedicat
 OpenBSD Cryptographic Framework | The OpenBSD Cryptographic Framework (OCF) is a ser
(7 rows)

Time: 1133.165 ms (00:01.133)
```

Notare i tempi relativamente lunghi: stiamo facendo 7 interrogazioni via WWW a wikipedia.org!


## Librerie di terze parti

Si può usare qualsiasi libreria di terze parti, se è installata in qualche cartella che l'installazione
di Python usa. Questo è dinamico, basta che la libreria sia presente al momento della chiamata della procedura.

Ecco un esempio che usa *numpy* per generare n numeri random distribuiti normalmente:

```text
CREATE OR REPLACE FUNCTION gaussian(n int)
  RETURNS float8[]
AS $$
    import numpy as np
    return np.random.normal(size=n)
$$ LANGUAGE plpython3u;
```

Notare come PL/Python mappa automaticamente un *ndarray* di *numpy* su un *float8[]* di Postgres.

Esempio d'uso:

```text
pgtraining=# select unnest(gaussian(10)) as nrand;
        nrand         
----------------------
 -0.12150432920328795
   0.8872284435919064
 -0.02796618428461806
  0.26041989062323334
   1.2063321272609637
  0.14927380662962378
     0.64640290200996
  -0.9453888007491033
   0.7594129690205649
    0.341385876148429
(10 rows)

Time: 0.565 ms
```


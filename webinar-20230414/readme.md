# Webinar 14 Aprile 2023

Breve resoconto dell'evento **online grauito** organizzato il giorno 14 Aprile 2023.

## Il linguaggio PL/Perl - Luca Ferrari

PL/Perl è l'integrazione in PostgreSQL del linguaggio Perl 5, grazie al quale è possibile scrivere funzioni, routine e trigger usando tutta la potenza di Perl direttamente all'interno del nostro database preferito!

Link utili:
- [video su Vimeo](https://vimeo.com/820564361) &check;
- [slide PDF](https://gitlab.com/pgtraining/slides/-/blob/master/webinar-20230414/2023_PGTRAINING_PLPERL.pdf) &check;
- [file di esempio](https://gitlab.com/pgtraining/slides/-/tree/master/webinar-20230414/examples/plperl/) &check;


## Il linguaggio PL/Python - Chris Mair

PL/Python è l'integrazione in PostgreSQL del linguaggio Python, grazie al quale è possibile scrivere funzioni, routine e trigger usando tutta la potenza di Python direttamente all'interno del nostro database preferito!

Link utili:
- [video su Vimeo](https://vimeo.com/817679597) &check;
- [appunti Markdown](https://gitlab.com/pgtraining/slides/-/blob/master/webinar-20230414/2023_PGTRAINING_PLPYTHON_V1.md) &check;

## Nuove funzionalità di PostgreSQL 15 - Enrico Pirozzi

PostgreSQL 15 è la versione stabile più recente del database Open Source di fascia Enterprise migliore al mondo. Ad ogni release, il team di sviluppatori aggiunge funzionalità e miglioramenti; in questo talk si andrà a scoprire le principali novità dell'ultima versione disponibile.

Link utili:
- [slide PDF](https://gitlab.com/pgtraining/slides/-/blob/master/webinar-20230414/pg15_new_features.pdf) &check;

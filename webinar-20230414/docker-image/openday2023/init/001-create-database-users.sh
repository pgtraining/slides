set -e
psql -U postgres -c "alter role postgres password 'pgday2023'" postgres
psql -U postgres -c "create role luca with login password  'pgday2023'" postgres
psql -U postgres -c "create role enrico with login password 'pgday2023'" postgres
psql -U postgres -c "create role chris with login password 'pgday2023'" postgres
